# Changelog

08-18-2019

- First commit: ansible, firmware, and documentation files.

08-19-2019

- Mirrored from Gitlab to UVA-DSI org. repo
- Added licensing info
- Cleaned up temp files
